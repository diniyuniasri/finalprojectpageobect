Feature: Order an item in Your Store

  @OrderNewAddress
  Scenario: User order an item in Your Store
    Given [SEOUL] I open Prestashop
    When [SEOUL] I click 'Sign In' in top bar
    When [SEOUL] I input Email and Password

    Given [SEOUL] I open Prestashop
    When [SEOUL] I click on product '6'
    When [SEOUL] I click '2' in 'Quantity'
    When [SEOUL] I click Add to Cart
    When [SEOUL] I click on 'Proceed To Checkout'
    When [SEOUL] I click Proceed To Checkout in Shopping Cart
    When [SEOUL] I click add new address

    When [SEOUL] I input 'Rizal' on 'First name'
    When [SEOUL] I input 'Fauzy' on 'Last name'
    When [SEOUL] I input 'PT. Graha Niaga Thamrin' on 'Company'
    When [SEOUL] I input '84.903.329.5-614.000' on 'VAT number'
    When [SEOUL] I input 'Jl. Raya ITS' on 'Address'
    When [SEOUL] I input 'PENS' on 'Address Complement'
    When [SEOUL] I input '60111' on 'Zip/Postal Code'
    When [SEOUL] I input 'Surabaya' on 'City'
    When [SEOUL] I choose 'Indonesia' on 'Country'
    When [SEOUL] I choose 'Jawa Timur' on 'State'
    When [SEOUL] I input '085648911250' on 'Phone'
    When [SEOUL] I click checkbox 'Use this address for invoice too'
    When [SEOUL] I click 'Continue' button in new Address

    When [SEOUL] I click radio button 'J&T' in Shipping Method
    When [SEOUL] I comment 'my house on 3rd floor' in Shipping Method
    When [SEOUL] I click 'Continue' button in Shipping Method
    When [SEOUL] I click radio button 'Pay by Bank Wire' in Payment
    When [SEOUL] I click checkbox 'I agree Terms & Condition' in Payment
    When [SEOUL] I click 'Order with Obligation To Pay' in Payment

    Given [SEOUL] I open Prestashop