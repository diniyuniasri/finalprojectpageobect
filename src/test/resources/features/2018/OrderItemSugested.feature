Feature: Order an item in Your Store

  @Order
  Scenario: User order an item in Your Store
    Given [SEOUL] I open Prestashop
    When [SEOUL] I click 'Sign In' in top bar
    When [SEOUL] I input Email and Password

    Given [SEOUL] I open Prestashop
    When [SEOUL] I type on search field 'brown bear cushion'
    When [SEOUL] I click on product '1' on search
    When [SEOUL] I click '2' in 'Quantity'
    When [SEOUL] I click Add to Cart

    When [SEOUL] I click on Proceed To Checkout
    When [SEOUL] I click Proceed To Checkout in Shopping Cart
    When [SEOUL] I click Continue button in Addresses
    When [SEOUL] I continue checkout proceed

    Given [SEOUL] I open Prestashop