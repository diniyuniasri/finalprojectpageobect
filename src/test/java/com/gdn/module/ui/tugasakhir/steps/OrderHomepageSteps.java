package com.gdn.module.ui.tugasakhir.steps;

import com.gdn.module.ui.tugasakhir.pages.OrderPage;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class OrderHomepageSteps {
    @Autowired
    OrderPage orderPage;

    @When("^\\[SEOUL\\] I click on product '(.*)'$")
    public void seoulIClickAddToCartOnProduct(int product) throws Throwable {
        orderPage.clickButtonProduct(product);
    }

    @When("^\\[SEOUL\\] I click Shopping Cart button$")
    public void seoulIClickShoppingCartButton() throws Throwable {
        orderPage.clickButtonCheckout();
    }

    @When("^\\[SEOUL\\] I click Add to Cart$")
    public void seoulIClickCheckoutInShoppingCart() throws Throwable {
        orderPage.clickAddToCart();
    }

    @When("^\\[SEOUL\\] I click '(.*)' in '(.*)'$")
    public void seoulIClickInQuantity(String input, String keyword) throws Throwable {
        switch (keyword){
            case "Quantity":
                orderPage.typeQuantity(input);
                break;

            case "Size":
                switch (input) {
                    case "S":
                        input = "1";
                        break;

                    case "M":
                        input = "2";
                        break;

                    case "L":
                        input = "2";
                        break;

                    case "XL":
                        input = "2";
                        break;
                }
                orderPage.clickSize(input);
        }
    }

    @When("^\\[SEOUL\\] I click on Proceed To Checkout$")
    public void seoulIClickProceedToCheckout() throws Throwable {
        orderPage.clickBtnCheckout();
    }

    @When("^\\[SEOUL\\] I click Proceed To Checkout in Shopping Cart$")
    public void seoulIClickProceedToCheckoutInShoppingCart() throws Throwable {
        orderPage.clickContinueCheckOut();
    }

    @When("^\\[SEOUL\\] I type on search field '(.*)'$")
    public void iHasPlacedTheFollowingItemsInHisShoppingCart(String input) throws Throwable {
        orderPage.typeOnSearch(input);
    }

    @When("^\\[SEOUL\\] I click on product '(.*)' on search$")
    public void seoulIClickOnProductOnSearch(int arg0) throws Throwable {
        orderPage.clickSearchResult();
    }
}