package com.gdn.module.ui.tugasakhir.steps;

import com.gdn.module.ui.tugasakhir.CucumberStepsDefinition;
import com.gdn.module.ui.tugasakhir.pages.OrdersBackendPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

@CucumberStepsDefinition
public class BackendSeeOrders {
    @Autowired
    OrdersBackendPage ordersBackendPage;

    @When("^\\[BACKEND\\] I click status '(.*)'$")
    public void backendIClickStatusPaymentAccepted(String input) throws Throwable {
        switch (input){
            case "Awaiting bank wire payment":
                ordersBackendPage.clickUpdateStatus(2);
                break;

            case "Awaiting Cash On Delivery validation":
                ordersBackendPage.clickUpdateStatus(3);
                break;

            case "Awaiting check payment":
                ordersBackendPage.clickUpdateStatus(4);
                break;

            case "Canceled":
                ordersBackendPage.clickUpdateStatus(5);
                break;

            case "Delivered":
                ordersBackendPage.clickUpdateStatus(6);
                break;

            case "On backorder (not paid)":
                ordersBackendPage.clickUpdateStatus(7);
                break;

            case "On backorder (paid)":
                ordersBackendPage.clickUpdateStatus(8);
                break;

            case "Payment accepted":
                ordersBackendPage.clickUpdateStatus(9);
                break;

            case "Payment error":
                ordersBackendPage.clickUpdateStatus(10);
                break;

            case "Processing in progress":
                ordersBackendPage.clickUpdateStatus(11);
                break;

            case "Refunded":
                ordersBackendPage.clickUpdateStatus(12);
                break;

            case "Remote payment accepted":
                ordersBackendPage.clickUpdateStatus(13);
                break;

            case "Shipped":
                ordersBackendPage.clickUpdateStatus(14);
                break;
        }
    }

    @When("^\\[BACKEND\\] I click button '(.*)'$")
    public void backendIClickButtonUpdateStatus(String keyword) throws Throwable {
        switch (keyword){
            case "Update Status":
                ordersBackendPage.clickBtnUpdateStatus();
                break;
        }
    }

    @Then("^\\[BACKEND\\] I see the '(.*)' is '(.*)'$")
    public void backendISeeStatusOnPaymentAccepted(String keyword, String param) throws Throwable {
        switch (keyword){
            case "Status":
                Assert.assertTrue("The Status is not match", ordersBackendPage.seeRowTable(2, param));
                break;

            case "Admin":
                Assert.assertTrue("The Admin is not match", ordersBackendPage.seeRowTable(3, param));
                break;

            case "Date":
                Assert.assertTrue("The Date is not match", ordersBackendPage.seeRowTable(4, param));
                break;

            default:
                Assert.assertFalse("Keyword false", false);
                break;
        }
    }
}
