package com.gdn.module.ui.tugasakhir.steps;

import com.gdn.module.ui.tugasakhir.CucumberStepsDefinition;
import com.gdn.module.ui.tugasakhir.pages.OrdersBackendPage;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

@CucumberStepsDefinition
public class BackendMenuSubmenuSteps {

    @Autowired
    OrdersBackendPage ordersBackendPage;

    @When("^\\[BACKEND\\] I click menu '(.*)' sub menu '(.*)'$")
    public void backendIClickMenuOrdersSubMenuOrders(String menu, String submenu) throws Throwable {
        switch (menu){
            case ("Orders"):
                ordersBackendPage.clickMenuSubmenu("subtab-AdminParentOrders");
                switch (submenu){
                    case ("Orders"):
                        ordersBackendPage.clickMenuSubmenu("subtab-AdminOrders");
                        break;

                    case ("Invoices"):
                        ordersBackendPage.clickMenuSubmenu("subtab-AdminInvoices");
                        break;

                    case ("Credit Slips"):
                        ordersBackendPage.clickMenuSubmenu("subtab-AdminSlip");
                        break;

                    case ("Delivery Slips"):
                        ordersBackendPage.clickMenuSubmenu("subtab-AdminDeliverySlip");
                        break;

                    case ("Shopping Carts"):
                        ordersBackendPage.clickMenuSubmenu("subtab-AdminCart");
                        break;
                }
                break;

            case ("Catalog"):
                ordersBackendPage.clickMenuSubmenu("subtab-AdminCatalog");
                break;

            case ("Customers"):
                ordersBackendPage.clickMenuSubmenu("subtab-AdminParentCustomer");
                break;

            case ("Customer Service"):
                ordersBackendPage.clickMenuSubmenu("subtab-AdminParentCustomerThreads");
                break;

            case ("Stats"):
                ordersBackendPage.clickMenuSubmenu("subtab-AdminStats");
                break;

            case ("Modules"):
                ordersBackendPage.clickMenuSubmenu("subtab-AdminParentModulesSf");
                break;

            case ("Design"):
                ordersBackendPage.clickMenuSubmenu("subtab-AdminParentThemes");
                break;

            case ("Shipping"):
                ordersBackendPage.clickMenuSubmenu("subtab-AdminParentShipping");
                break;

            case ("Payment"):
                ordersBackendPage.clickMenuSubmenu("subtab-AdminParentPayment");
                break;

            case ("International"):
                ordersBackendPage.clickMenuSubmenu("subtab-AdminInternational");
                break;

            case ("Shop Parameters"):
                ordersBackendPage.clickMenuSubmenu("subtab-ShopParameters");
                break;

            case ("Advance Parameters"):
                ordersBackendPage.clickMenuSubmenu("subtab-AdminAdvancedParameters");
                break;
        }
    }
}
