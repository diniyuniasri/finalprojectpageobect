package com.gdn.module.ui.tugasakhir.steps;

import com.gdn.module.ui.tugasakhir.CucumberStepsDefinition;
import com.gdn.module.ui.tugasakhir.pages.BackendLoginPage;
import com.gdn.module.ui.tugasakhir.properties.BlibliProperties;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

@CucumberStepsDefinition
public class BackendLoginSteps {
    @Autowired
    BackendLoginPage loginPage;

    @Autowired
    BlibliProperties prop;

    @Given("^\\[BACKEND\\] I open Prestashop$")
    public void backendIOpenPrestashop() throws Throwable {
        loginPage.openAt("http://localhost/prestashop/admin077jisykc/");
//        loginPage.openAt("http://localhost/prestashop/prestashop/admin811uvktyn/");
    }

    @When("^\\[BACKEND\\] I input Email and Password$")
    public void backendIInputEmailAndPassword() throws Throwable {
        loginPage.inputUsername(prop.backend.getUsername());
        loginPage.inputPassword(prop.backend.getPassword());
        loginPage.clickBtnLogin();
    }
}
