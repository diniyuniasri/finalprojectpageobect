package com.gdn.module.ui.tugasakhir.steps;

import com.gdn.module.ui.tugasakhir.pages.CheckoutPage;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class BillingDetailsSteps {
    @Autowired
    CheckoutPage checkoutPage;

    @When("^\\[SEOUL\\] I click Continue button in Addresses$")
    public void seoulIClickContinueButtonInAddresses() throws Throwable {
        checkoutPage.clickContinueAddresses();
    }

    @When("^\\[SEOUL\\] I continue checkout proceed$")
    public void seoulIContinueCheckoutProceed(String input) throws Throwable {
        checkoutPage.clickCourier();
        checkoutPage.clickContinueShippingMethod();
        checkoutPage.clickRadioPayment();
        checkoutPage.clickCheckboxTermCond();
        checkoutPage.clickOrderButton();
    }
/*
    @When("^\\[SEOUL\\] I comment '(.*)' in Shipping Method$")
    public void seoulICommentMyHouseOnRdFloorInShippingMethod(String input) throws Throwable {
        checkoutPage.inputCommentShippingMethod(input);
    }*/

    /*@When("^\\[SEOUL\\] I click '(.*)' button in Shipping Method$")
    public void seoulIClickContinueButtonInShippingMethod(String input) throws Throwable {
        switch (input){
            case "Continue":
                checkoutPage.clickContinueShippingMethod();
                break;
        }
    }*/

    /*@When("^\\[SEOUL\\] I click radio button '(.*)' in Payment$")
    public void seoulIClickRadioButtonPayByBankWireInPayment(String input) throws Throwable {
        switch (input){
            case "Pay by Bank Wire":
                checkoutPage.clickRadioPayment(2);
                break;

            case "Pay by Check":
                checkoutPage.clickRadioPayment(1);
                break;
        }
    }*/

    /*@When("^\\[SEOUL\\] I click checkbox '(.*)' in Payment$")
    public void seoulIClickCheckboxIAgreeTermsConditionInPayment(String input) throws Throwable {
        switch (input){
            case "I agree Terms & Condition":
                checkoutPage.clickCheckboxTermCond();
                break;
        }
    }*/

    /*@When("^\\[SEOUL\\] I click '(.*)' in Payment$")
    public void seoulIClickOrderWithObligationToPayInPayment(String input) throws Throwable {
        switch (input){
            case "Order with Obligation To Pay":
                checkoutPage.clickOrderButton();
                break;
        }
    }*/

    @When("^\\[SEOUL\\] I click add new address$")
    public void seoulIClickAddNewAddress() throws Throwable {
        checkoutPage.clickAddAddress();
    }

    @When("^\\[SEOUL\\] I input '(.*)' on '(.*)'$")
    public void seoulIInputDiniOnFirstName(String input, String keyword) throws Throwable {
        switch (keyword){
            case "First name":
                checkoutPage.inputFormNewAddress(input, 1);
                break;

            case "Last name":
                checkoutPage.inputFormNewAddress(input, 2);
                break;

            case "Company":
                checkoutPage.inputFormNewAddress(input, 3);
                break;

            case "VAT number":
                checkoutPage.inputFormNewAddress(input, 4);
                break;

            case "Address":
                checkoutPage.inputFormNewAddress(input, 5);
                break;

            case "Address Complement":
                checkoutPage.inputFormNewAddress(input, 6);
                break;

            case "Zip/Postal Code":
                checkoutPage.inputFormNewAddress(input, 7);
                break;

            case "City":
                checkoutPage.inputFormNewAddress(input, 8);
                break;

            case "Phone":
                checkoutPage.inputFormNewAddress(input, 11);
                break;
        }
    }

    @When("^\\[SEOUL\\] I choose '(.*)' on '(.*)'$")
    public void seoulIChooseIndonesiaOnCountry(String input, String keyword) throws Throwable {
        switch (keyword){
            case "State":
                checkoutPage.chooseState(input);
                break;

            case "Country":
                checkoutPage.chooseCountry(input);
                break;
        }
    }

    @When("^\\[SEOUL\\] I click checkbox '(.*)'$")
    public void seoulIClickCheckboxUseThisAddressForInvoiceToo(String keyword) throws Throwable {
        switch (keyword){
            case "Use this address for invoice too":
                checkoutPage.clickCheckboxUseAddress();
                break;
        }
    }

    @When("^\\[SEOUL\\] I click Continue button in new Address$")
    public void seoulIClickContinueButtonInNewAddress() throws Throwable {
        checkoutPage.clickForNewAddress();
    }
}