package com.gdn.module.ui.tugasakhir.steps;

import com.gdn.module.ui.tugasakhir.CucumberStepsDefinition;
import com.gdn.module.ui.tugasakhir.pages.OrdersBackendPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

@CucumberStepsDefinition
public class BackendSubmenuOrders {

    @Autowired
    OrdersBackendPage ordersBackendPage;

    @When("^\\[BACKEND\\] I click dropdown '(.*)' on '(.*)'$")
    public void backendIClickAwaitingBankWirePaymentOnStatus(String input, String keyword) throws Throwable {
        switch (keyword){
            case "New Client":
                switch (input){
                    case "yes":
                        ordersBackendPage.clickdropdownTHead(4, 2);
                        break;

                    case "no":
                        ordersBackendPage.clickdropdownTHead(4, 3);
                        break;
                }
                break;

            case "Delivery":
                switch (input){
                    case "Indonesia":
                        ordersBackendPage.clickdropdownTHead(5, 2);
                        break;

                    case "United States":
                        ordersBackendPage.clickdropdownTHead(5, 3);
                        break;
                }
                break;

            case "Status":
                switch (input){
                    case "Awaiting bank wire payment":
                        ordersBackendPage.clickdropdownTHead(9, 2);
                        break;

                    case "Awaiting Cash On Delivery validation":
                        ordersBackendPage.clickdropdownTHead(9, 3);
                        break;

                    case "Awaiting check payment":
                        ordersBackendPage.clickdropdownTHead(9, 4);
                        break;

                    case "Canceled":
                        ordersBackendPage.clickdropdownTHead(9, 5);
                        break;

                    case "Delivered":
                        ordersBackendPage.clickdropdownTHead(9, 6);
                        break;

                    case "On backorder (not paid)":
                        ordersBackendPage.clickdropdownTHead(9, 7);
                        break;

                    case "On backorder (paid)":
                        ordersBackendPage.clickdropdownTHead(9, 8);
                        break;

                    case "Payment accepted":
                        ordersBackendPage.clickdropdownTHead(9, 9);
                        break;

                    case "Payment error":
                        ordersBackendPage.clickdropdownTHead(9, 10);
                        break;

                    case "Processing in progress":
                        ordersBackendPage.clickdropdownTHead(9, 11);
                        break;

                    case "Refunded":
                        ordersBackendPage.clickdropdownTHead(9, 12);
                        break;

                    case "Remote payment accepted":
                        ordersBackendPage.clickdropdownTHead(9, 13);
                        break;

                    case "Shipped":
                        ordersBackendPage.clickdropdownTHead(9, 14);
                        break;
                }
                break;

            case "Date":
                break;
        }
    }

    @When("^\\[BACKEND\\] I type '(.*)' on '(.*)'$")
    public void backendITypeAMXXBTNHTOnReference(String input, String keyword) throws Throwable {
        switch (keyword){
            case "ID":
                ordersBackendPage.typeThead(2, input);
                break;

            case "Reference":
                ordersBackendPage.typeThead(3, input);
                break;

            case "Customer":
                ordersBackendPage.typeThead(6, input);
                break;

            case "Total":
                ordersBackendPage.typeThead(7, input);
                break;

            case "Payment":
                ordersBackendPage.typeThead(8, input);
                break;
        }
    }

    @Then("^\\[BACKEND\\] I click '(.*)'$")
    public void backendIClickSearch(String keyword) throws Throwable {
        switch (keyword){
            case "Search":
                ordersBackendPage.clickBtnSearch();
                break;

            case "Reset":
                ordersBackendPage.clickBtnReset();
                break;
        }

    }

    @Then("^\\[BACKEND\\] I click row number '(.*)'$")
    public void backendIClickRowNumber(String input) throws Throwable {
        ordersBackendPage.clickRow(input);
    }
}
