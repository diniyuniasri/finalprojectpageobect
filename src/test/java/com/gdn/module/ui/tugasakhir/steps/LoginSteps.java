package com.gdn.module.ui.tugasakhir.steps;

import com.gdn.module.ui.tugasakhir.CucumberStepsDefinition;
import com.gdn.module.ui.tugasakhir.pages.LoginPage;
import com.gdn.module.ui.tugasakhir.properties.BlibliProperties;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

@CucumberStepsDefinition
public class LoginSteps {
    @Autowired
    LoginPage loginPage;

    @Autowired
    BlibliProperties prop;

    @Given("^\\[SEOUL\\] I open Prestashop$")
    public void seoulIOpenSeoul() throws Throwable {
        loginPage.openAt("http://localhost/prestashop/en/");
    }

    @When("^\\[SEOUL\\] I input Email and Password$")
    public void seoulIInputUsernameAndPassword() throws Throwable {
        loginPage.inputUsername(prop.user.getUsername());
        loginPage.inputPassword(prop.user.getPassword());
        loginPage.clickBtnLogin();
    }

    @When("^\\[SEOUL\\] I click '(.*)' in top bar")
    public void seoulIVerifyNoImage(String keyword) throws Throwable {
        switch (keyword){
            case "Contact Us":
                loginPage.clickTopBarMenu("contact-link");
                break;

            case "Sign In":
                loginPage.clickTopBarMenu("_desktop_user_info");
                break;

            case "Cart":
                loginPage.clickTopBarMenu("_desktop_cart");
                break;
        }
    }
}
