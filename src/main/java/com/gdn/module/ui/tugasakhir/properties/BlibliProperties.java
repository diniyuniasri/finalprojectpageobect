package com.gdn.module.ui.tugasakhir.properties;

import com.gdn.module.ui.tugasakhir.data.Backend;
import com.gdn.module.ui.tugasakhir.data.User;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties (prefix = "blibli")
public class BlibliProperties {
    public User user;
    public Backend backend;
}
