package com.gdn.module.ui.tugasakhir;

import com.gdn.module.ui.tugasakhir.properties.BlibliProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableConfigurationProperties({
        BlibliProperties.class
})
@ComponentScan(basePackages = {"com.gdn.module.ui", "net.thucydides", "net.serenitybdd" , })
public class Application {
    public Application(){
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}