package com.gdn.module.ui.tugasakhir.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import org.springframework.stereotype.Component;

@Component
public class BackendLoginPage extends PageObject {
    String usernameField = "//*[@name=\"email\"]";
    public void inputUsername(String input){
        System.out.println("tes");
        find(By.xpath(usernameField)).type(input);
    }

    String passwordField = "//*[@name=\"passwd\"]";
    public void inputPassword(String input) {
        find(By.xpath(passwordField)).type(input);
    }

    String btnLogin = "//*[@class=\"btn btn-primary btn-lg btn-block ladda-button\"]";
    public void clickBtnLogin(){
        find(By.xpath(btnLogin)).click();
    }
}
