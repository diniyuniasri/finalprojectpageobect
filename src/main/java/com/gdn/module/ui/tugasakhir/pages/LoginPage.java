package com.gdn.module.ui.tugasakhir.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
//import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

@Component
public class LoginPage extends PageObject {
    String topBarMenu1 = "//*[@id=\"";
    String topBarMenu2 = "\"]";
    public void clickTopBarMenu(String input){
        String topBarMenu = topBarMenu1 + input + topBarMenu2;
        find(By.xpath(topBarMenu)).click();
    }

    String usernameField = "//*[@name=\"email\"]";
    public void inputUsername(String input){
        find(By.xpath(usernameField)).type(input);
    }

    String passwordField = "//*[@name=\"password\"]";
    public void inputPassword(String input) {
        find(By.xpath(passwordField)).type(input);
    }

    String btnLogin = "//*[@class=\"btn btn-primary\"]";
    public void clickBtnLogin(){
        find(By.xpath(btnLogin)).click();
    }
}
