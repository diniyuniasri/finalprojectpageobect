package com.gdn.module.ui.tugasakhir.data;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class User {
    private String username;
    private String password;
}
