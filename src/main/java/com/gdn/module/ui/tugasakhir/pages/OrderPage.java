package com.gdn.module.ui.tugasakhir.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;

public class OrderPage extends PageObject {
    String buttonProduct1 = "//*[@id=\"content\"]/section/div/article[";
    String buttonProduct2 = "]/div/div[1]/h3/a";
    public void clickButtonProduct(int product){
        String buttonProduct = buttonProduct1 + product + buttonProduct2;
        find(By.xpath(buttonProduct)).click();
    }

    String buttonCheckout = "//*[@id=\"cart\"]/button";
    public void clickButtonCheckout(){
        find(By.xpath(buttonCheckout)).click();
    }

    String inputUpdown = "//*[@id=\"quantity_wanted\"]";
    public void typeQuantity(String input){
        find(By.xpath(inputUpdown)).type(input);
    }

    String inputSize1 = "//*[@id=\"group_1\"]/option[";
    String inputSize2 = "]";
    public void clickSize(String input){
        find(By.xpath(inputSize1 + input +inputSize2));
    }

    String addToCart = "//*[@id=\"add-to-cart-or-refresh\"]/div[2]/div/div[2]/button";
    public void clickAddToCart() {
        waitABit(1000);
        find(By.xpath(addToCart)).click();
    }

    String proceedToCheckout = "//*[@id=\"blockcart-modal\"]/div/div/div[2]/div/div[2]/div/div/a";
    public void clickBtnCheckout(){
        waitABit(3000);
        find(By.xpath(proceedToCheckout)).click();
    }

    String continueShopping = "//*[@id=\"blockcart-modal\"]/div/div/div[2]/div/div[2]/div/div/button";
    public void clickContinueShop(){
        waitABit(1000);
        find(By.xpath(continueShopping)).click();
    }

    String continueCheckOut = "//*[@id=\"main\"]/div/div[2]/div[1]/div[2]/div/a";
    public void clickContinueCheckOut(){
        find(By.xpath(continueCheckOut)).click();
    }

    String searchBar = "//*[@id=\"search_widget\"]/form/input[2]";
    public void typeOnSearch(String input){
        find(By.xpath(searchBar)).typeAndEnter(input);
    }

    String searchResult = "//*[@id=\"js-product-list\"]/div[1]/article[1]/div/div[1]/h2";
    public void clickSearchResult(){
        find(By.xpath(searchResult)).click();
    }
}
