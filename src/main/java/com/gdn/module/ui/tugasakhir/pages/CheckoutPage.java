package com.gdn.module.ui.tugasakhir.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;

public class CheckoutPage extends PageObject {
    String continueBtnAddresses = "//*[@id=\"checkout-addresses-step\"]/div/div/form/div[2]/button";
    public void clickContinueAddresses(){
        find(By.xpath(continueBtnAddresses)).click();
    }

    String radioButtonCourier = "//*[@id=\"js-delivery\"]/div/div[1]/div[1]/div/span[1]";
    public void clickCourier(){
        waitABit(1500);
        find(By.xpath(radioButtonCourier)).click();
    }

    String commentShippingMethod = "//*[@id=\"delivery_message\"]";
    public void inputCommentShippingMethod(String input){
        find(By.xpath(commentShippingMethod)).type(input);
    }

    String continueBtnShippingMethod = "//*[@id=\"js-delivery\"]/button";
    public void clickContinueShippingMethod(){
        find(By.xpath(continueBtnShippingMethod)).click();
    }

    String radioPayment = "//*[@id=\"payment-option-2\"]";
    String radioPayment2 = "\"]";
    public void clickRadioPayment (){
//        String radioPayment =radioPayment1 + input + radioPayment2;
        find(By.xpath(radioPayment)).click();
    }

    String checkboxTermCond = "//*[@id=\"conditions_to_approve[terms-and-conditions]\"]";
    public void clickCheckboxTermCond (){
        find(By.xpath(checkboxTermCond)).click();
    }

    String orderButton = "//*[@id=\"payment-confirmation\"]/div[1]/button";
    public void clickOrderButton(){
        find(By.xpath(orderButton)).click();
    }

    String addAddress = "//*[@id=\"checkout-addresses-step\"]/div/div/form/p[3]/a";
    public void clickAddAddress(){
        find(By.xpath(addAddress)).click();
    }

    String checkboxUseAddress = "//*[@id=\"delivery-address\"]/div/section/div[12]/div/input";
    public void clickCheckboxUseAddress(){
        find(By.xpath(checkboxUseAddress)).click();
    }

    String formNewAddress1 = "//*[@id=\"delivery-address\"]/div/section/div[";
    String formNewAddress2 = "]/div[1]/input";
    public void inputFormNewAddress(String input, int keyword){
        String formNewAddres = formNewAddress1 + keyword + formNewAddress2;
        find(By.xpath(formNewAddres)).type(input);
    }

    String dropState1 = "//*[@id=\"delivery-address\"]/div/section/div[9]/div[1]/select/option[text()=\"";
    String dropState2 = "\"]";
    public void chooseState(String input){
        String dropState = dropState1 + input + dropState2;
        find(By.xpath(dropState)).click();
    }

    String dropCountry1 = "//*[@id=\"delivery-address\"]/div/section/div[10]/div[1]/select/option[text()=\"";
    String dropCountry2 = "\"]";
    public void chooseCountry(String input){
        String dropCountry = dropCountry1 + input + dropCountry2;
        find(By.xpath(dropCountry)).click();
    }

    String newAddress = "//*[@id=\"delivery-address\"]/div/footer/button";
    public void clickForNewAddress(){
//        String clickObject = newAddress + input;
        find(By.xpath(newAddress)).click();
    }
}
