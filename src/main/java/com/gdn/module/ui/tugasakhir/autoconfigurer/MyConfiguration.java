package com.gdn.module.ui.tugasakhir.autoconfigurer;

import com.gdn.module.ui.tugasakhir.properties.BlibliProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({
        BlibliProperties.class
})
public class MyConfiguration {
}
