package com.gdn.module.ui.tugasakhir.pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

@Component
public class OrdersBackendPage extends PageObject {
    String menu1 = "//*[@id=\"";
    String menu2 = "\"]";
    public void clickMenuSubmenu(String input){
        String menu = menu1 +input + menu2;
        find(By.xpath(menu)).click();
    }

    String tHead = "//*[@id=\"table-order\"]/thead/tr[2]/th[";
    String dropdownTHead = "]/select/option[";
    public void clickdropdownTHead(int keyword, int input){
        String dropdown = tHead + keyword + dropdownTHead + input + "]";
        find(By.xpath(dropdown)).click();
    }

    String inputTHead = "]/input";
    public void typeThead(int keyword, String input){
        String inputArea = tHead + keyword + inputTHead;
        find(By.xpath(inputArea)).type(input);
    }

    String btnSearch = "//*[@id=\"submitFilterButtonorder\"]";
    public void clickBtnSearch(){
        find(By.xpath(btnSearch)).click();
    }

    String btnReset = "//*[@id=\"table-order\"]/thead/tr[2]/th[12]/span/button[2]";
    public void clickBtnReset(){
        find(By.xpath(btnReset)).click();
    }

    String row = "//*[@id=\"table-order\"]/tbody/tr[";
    public void clickRow (String input){
        find(By.xpath(row + input + "]")).click();
    }

    String updateStatus1 = "//*[@id=\"id_order_state_chosen\"]/a";
//    String updateStatus1 = "//*[@id=\"status\"]/form/div";
    String updateStatus = "/div/ul/div/input";
    public void clickUpdateStatus(int input){
        waitABit(5000);
        withAction().moveToElement(find(By.xpath(updateStatus1)))
                .click()
                .release()
                .build()
                .perform();

        /*find(By.xpath(updateStatus1)).click();
        waitABit(5000);
//        find(By.xpath(updateStatus1 + updateStatus)).type("Payment Error");
//        find(By.xpath(updateStatus + input + "]")).click();*/
        waitABit(5000);
    }

    String btnUpdateStatus = "//*[@id=\"submit_state\"]";
    public void clickBtnUpdateStatus(){
        find(By.xpath(btnUpdateStatus)).click();
    }

    String rowTable = "//*[@id=\"status\"]/div/table/tbody/tr[1]/td[";
    public boolean seeRowTable(int input, String param){
        String row = find(By.xpath(rowTable + input + "]")).getText();
        if(row.equals(param))
            return true;
        else
            return false;
    }
}
