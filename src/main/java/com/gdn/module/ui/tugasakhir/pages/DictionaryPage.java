package com.gdn.module.ui.tugasakhir.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

import java.util.List;

//@DefaultUrl("http://en.wiktionary.org/wiki/Wiktionary")
@Component
public class DictionaryPage extends PageObject {

    @FindBy(name="search")
    private WebElementFacade searchTerms;

    @FindBy(name="go")
    private WebElementFacade lookupButton;

    public void enter_keywords(String keyword) {
       // searchTerms.type(keyword);
    }

    public void lookup_terms() {
        //lookupButton.click();
    }

    public List<String> getDefinitions() {
      /*  WebElementFacade definitionList = find(By.tagName("ol"));
        return definitionList.findElements(By.tagName("li")).stream()
                             .map( element -> element.getText() )
                             .collect(Collectors.toList());*/
      return null;
    }
}